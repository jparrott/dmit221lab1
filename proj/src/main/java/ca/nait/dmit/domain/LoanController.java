package ca.nait.dmit.domain;
import java.io.Serializable;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.ViewScoped;

import org.primefaces.model.chart.CartesianChartModel;
import org.primefaces.model.chart.ChartSeries;






@ManagedBean(name = "loanBean")
@ViewScoped
public class LoanController implements Serializable{
	
	private Loan loan = new Loan();
	
	
	
	private CartesianChartModel categoryModel = new CartesianChartModel();  
	  
	
	
    public void setCategoryModel(CartesianChartModel categoryModel) {
		this.categoryModel = categoryModel;
	}

	
    public CartesianChartModel getCategoryModel() {  
        return categoryModel;  
    }  

	public Loan getloan()
	{
		return loan;		
	}
	
	public void setLoan(Loan loan)
	{
		this.loan = loan;
	}
	
	public void calculatePayment()
	{
		JSFUtil.addInfoMessage(
				"Your Monthly Payment is: "
				+loan.getMonthlyPayment()
				);
		createCategoryModel();
		//loanScheduleTable = loan.getLoanScheduleArray();
	}
	
	private void createCategoryModel() {  
        
		int count = 12;
		LoanSchedule[] loanDerp =  loan.getLoanScheduleArray();
        ChartSeries loanC = new ChartSeries();  
        loanC.setLabel("Year");  
  
//        boys.set("2004", 120);
        while(count < loanDerp.length)
        {
        	loanC.set(count, loanDerp[count].getRemainingBalance());
        	if(count == 300)
        	{
        		loanC.set(count, 0);
        	}
        	count += 12;
        }
        	
      // loanC.set(year, loan.getMonthlyPayment());
        categoryModel.addSeries(loanC); 
        
  
       // categoryModel.addSeries(year);  
       
    }  
//	public int getIntPaid()
//	{
//		int intPaid = 0;
//		
//		
//		return intPaid;
//	}
}
