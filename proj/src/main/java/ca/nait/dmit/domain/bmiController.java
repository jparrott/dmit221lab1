package ca.nait.dmit.domain;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;



@ManagedBean(name = "bmiBean")
@RequestScoped
public class bmiController {
	private BMI bmi = new BMI();
			
	public BMI getBmi() {
		return bmi;
	}



	public void setBmi(BMI bmi) {
		this.bmi = bmi;
	}

	public void calculateBMI()
	{
		if(bmi.getBMICategory() == "normal")
		{
		JSFUtil.addInfoMessage(
				"Your BMI is: "
				+bmi.getBMI() + ". You are categorized as " + bmi.getBMICategory()
				);
		}
		else if (bmi.getBMICategory() == "overweight")
		{
			JSFUtil.addWarningMessage(
					"Warning! Your BMI is: "
					+bmi.getBMI() + ". You are categorized as " + bmi.getBMICategory()
					);
		}
		else if (bmi.getBMICategory() == "obese")
		{
			JSFUtil.addErrorMessage(
					"Danger! Your BMI is: "
					+bmi.getBMI() + ". You are categorized as " + bmi.getBMICategory()
					);
		}
		else if (bmi.getBMICategory() == "underweight")
		{
			JSFUtil.addErrorMessage(
					"Danger! Your BMI is: "
					+bmi.getBMI() + ". You are categorized as " + bmi.getBMICategory()
					);
		}
	}
}