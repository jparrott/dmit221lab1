package ca.nait.dmit.domain;
import javax.validation.constraints.*;


/**
 * This class is use to calculate a person's body mass index (BMI) and their BMI Category.
 * @author James Parrott
 * @version 2012.09.15
 */
public class BMI {
	
	@Min(value = 40, message="Weight must be atleast 40lbs")
	private int weight;
	
	@Min(value = 3, message="Height must be atleast 3Ft")
	private int heightFeet;
	
	@Max(value = 11, message ="Height inches must be less than 12 inches")
	private double heightInches;
	
		
	public BMI() {
		super();
		// TODO Auto-generated constructor stub
	}
	
		
	public BMI(int weight, int heightFeet, double heightInches) {
		super();
		this.weight = weight;
		this.heightFeet = heightFeet;
		this.heightInches = heightInches;
	}
		
	public int getWeight() {
		return weight;
	}

	public void setWeight(int weight) {
		this.weight = weight;
	}

	public int getHeightFeet() {
		return heightFeet;
	}

	public void setHeightFeet(int heightFeet) {
		this.heightFeet = heightFeet;
	}

	public double getHeightInches() {
		return heightInches;
	}
	
	public void setHeightInches(double heightInches) {
		this.heightInches = heightInches;
	}
	
	/**
	 Calculate the height in inches by grabbing both values and combining them
	 @return the height value in inches
	 */
	public double calcHeightInches()
	{
		double mainHeight = 0;
		
		mainHeight = (getHeightFeet() * 12) + getHeightInches();
		
		return mainHeight;
	}
	
	/**
	 * Calculate the body mass index (BMI) using the weight and height of the person.
	 * The BMI of a person is calculated using the formula: BMI = 700 * weight / (height * height)
	 * where weight is in pounds and height is in inches. 
	 * @return the body mass index (BMI) value of the person
	 */
	public double getBMI()
	{		
		return 703*getWeight()/Math.pow(calcHeightInches(), 2);	
	}	
	
	/**
	 * Determines the BMI Category of the person using their BMI value and comparing
	 * it against the following table.
	 * -----------------------------------------
	 * | BMI range			        | BMI Category |
	 * |---------------------------------------|
	 * | < 18.5 			        | underweight	 |
	 * | >= 18.5 and < 25	| normal		   |
	 * | >= 25 and < 30		| overweight	 |
	 * | >= 30				        | obese			   |
	 * -----------------------------------------
	 * @return one of following: underweight, normal, overweight, obese.
	 */
	public String getBMICategory()
	{
		String category = "calculation mistake";
		double bmi = getBMI();
		if (bmi < 18.5)
		{
			category = "underweight";
		}
		if (bmi >= 18.5 && bmi <24.9)
		{
			category = "normal";
		}
		if (bmi >= 25 && bmi < 29.9)
		{
			category = "overweight";
		}
		if (bmi > 30)
		{
			category = "obese";
		}
		return category;		
	}
}
